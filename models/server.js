
const express = require('express')
var cors = require('cors');
const { dbConnection } = require('../database/config');


class Server {
    constructor() {

        this.app = express();
        this.port = process.env.PORT;
        this.usariosPath = '/api/usuarios';
        //conectar a la base de datos
        this.conectarDB();

        //middlewares
        this.middlewares();

        //rutas de mi apps
        this.routes();
    }


    async conectarDB() {
        await dbConnection();
    }

    middlewares() {
        this.app.use(cors());
        //lectura y parseo del body
        this.app.use(express.json());

        //directorio Público
        this.app.use(express.static('public'));
    }
    routes() {
        this.app.use(this.usariosPath, require('../routes/usuarios'))

    }
    listen() {

        this.app.listen(this.port, () => {
            console.log('Servidor corriendo en puerto', this.port);
        });
    }
}

module.exports = Server;